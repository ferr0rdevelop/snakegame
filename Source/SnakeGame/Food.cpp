// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "PlayerPawnBase.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			this->Destroy();
			//SpawnItem(FoodClass);
		}
	}
}


/*
void AFood::SpawnItem(UClass* ItemToSpawn)
{
	float XCoordinate = FMath::FRandRange(-470.f, 470.f);
	float YCoordinate = FMath::FRandRange(-470.f, 470.f);
	FVector Location(XCoordinate, YCoordinate, 30.f);
	FRotator Rotation(0.f, 0.f, 0.f);

	GetWorld()->SpawnActor<AActor>(ItemToSpawn, Location, Rotation);
}
*/

