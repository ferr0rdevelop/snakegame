// Fill out your copyright notice in the Description page of Project Settings.


#include "Blockage.h"
#include "SnakeBase.h"

// Sets default values
ABlockage::ABlockage()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ABlockage::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ABlockage::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ABlockage::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->Destroy();
			UE_LOG(LogTemp, Error, TEXT("Snake overlap with blockage! Game over!"));
		}
	}
}
