// Fill out your copyright notice in the Description page of Project Settings.


#include "Room.h"
#include "Blockage.h"
#include "DrawDebugHelpers.h"
#include "UObject/UObjectBaseUtility.h"
#include "Debuff.h"
#include "Food.h"

DEFINE_LOG_CATEGORY_STATIC(LogSnake, All, All)

// Sets default values
ARoom::ARoom()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Floor = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("FloorComponent"));
	Timer = 15.f;
	
	DebuffLifeOnRoomTime = 8.f;
	DebuffOnSnakeTime = 3.f;
	Padding = 60.f;
	GridSizeX = 5;
	GridSizeY = 5;
	SquareWidth = 200.f;
    TopLeft = FVector(500.f, -500.f, 0.f);
    BottomRight = FVector(-500.f, 500.f, 0.f);
	GridHeight = 1.f;
	RoomLength = 1000.f;
	RoomWidth = 1000.f;
	

}

// Called when the game starts or when spawned
void ARoom::BeginPlay()
{
	Super::BeginPlay();
	//CreateGrid();

	GetWorldTimerManager().SetTimer(DebuffTimerHandle, this, &ARoom::OnTimerFired, Timer, true);	
	Food = SpawnItem(FoodClass);
	SpawnItem(BlockageClass);
	SpawnItem(BlockageClass);
	SpawnItem(BlockageClass);
	SpawnItem(BlockageClass);
	

	
}

// Called every frame
void ARoom::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (!IsValid(Food))
	{
		Food = SpawnItem(FoodClass);
		UE_LOG(LogSnake, Warning, TEXT("Spawn new Food"));
	}
}

void ARoom::OnTimerFired()
{
	if (FMath::RandBool()) {
		Debuff1 = SpawnItem(DebuffClass1);
		if (IsValid(Debuff1))
		{
			UE_LOG(LogSnake, Error, TEXT("Debuff1 spawned name: %s"), *Debuff1->GetName());
			Debuff1->SetLifeSpan(DebuffLifeOnRoomTime);
			UE_LOG(LogSnake, Error, TEXT("Debuff1 coordinate: X = %f , Y = %f"), ItemCoords[0][0], ItemCoords[0][1]);
		}
	}
	else
	{
		Debuff2 = SpawnItem(DebuffClass2);	
		if (IsValid(Debuff2))
		{
			UE_LOG(LogSnake, Error, TEXT("Debuff2 spawned name: %s"), *Debuff2->GetName());
			Debuff2->SetLifeSpan(DebuffLifeOnRoomTime);
			UE_LOG(LogSnake, Error, TEXT("Debuff1 coordinate: X = %f , Y = %f"), ItemCoords[0][0], ItemCoords[0][1]);
		}
	}
}

void ARoom::CreateGrid()
{
	for (int32 i = 0; i < GridSizeX; i++)
	{
		FVector Start = TopLeft + FVector(i * SquareWidth, -500.f, GridHeight);
		FVector End = Start + FVector(0.f, RoomLength, GridHeight);
		DrawDebugLine(GetWorld(), Start, End, FColor::Blue, true);
	}
}

AActor* ARoom::SpawnItem(UClass* ItemToSpawn)
{
	float XCoordinate = FMath::FRandRange(-470.f, 470.f);
	float YCoordinate = FMath::FRandRange(-470.f, 470.f);
	
	if (ItemToSpawn->IsChildOf(ADebuff::StaticClass()))
	{
		UE_LOG(LogSnake, Error, TEXT("CAST TO DEBUFF SUCCESS!!"));
		ItemCoords[0][0] = XCoordinate;
		ItemCoords[0][1] = YCoordinate;
	}
	else if (ItemToSpawn->IsChildOf(AFood::StaticClass()))
	{
		UE_LOG(LogSnake, Error, TEXT("CAST TO FOOD SUCCESS!!"));
		ItemCoords[1][0] = XCoordinate;
		ItemCoords[1][1] = YCoordinate;
	}

	else if (ItemToSpawn->IsChildOf(ABlockage::StaticClass()))
	{
		UE_LOG(LogSnake, Error, TEXT("CAST TO BLOCKAGE SUCCESS!!"));
		ItemCoords[2][0] = XCoordinate;
		ItemCoords[2][1] = YCoordinate;
	}
	
	FVector Location(XCoordinate, YCoordinate, 30.f);
	FRotator Rotation(0.f, 0.f, 0.f);
	return GetWorld()->SpawnActor<AActor>(ItemToSpawn, Location, Rotation);
}

