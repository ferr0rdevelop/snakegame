// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Debuff.h"
#include "TimerManager.h"
#include "Room.generated.h"

UCLASS()
class SNAKEGAME_API ARoom : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ARoom();

	float ItemCoords[3][2];

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	

private:

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = Room, meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* Floor;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = Room, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<AActor> FoodClass;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = Room, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<AActor> BlockageClass;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = Room, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<AActor> DebuffClass1;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = Room, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<AActor> DebuffClass2;

	UPROPERTY(EditAnyWhere, Category = Debuff)
	float Timer;

	// How long will the debuff stay in the room
	UPROPERTY(EditAnyWhere, Category = Debuff)
	float DebuffLifeOnRoomTime;

	// Time how long the debuff on the snake will last
	UPROPERTY(EditAnyWhere, Category = Debuff)
	float DebuffOnSnakeTime;

	FTimerHandle DebuffTimerHandle;

	void OnTimerFired();
	void CreateGrid();

	float SquareWidth;
	float GridHeight;
	float RoomLength;
	float RoomWidth;
	int32 GridSizeX;
	int32 GridSizeY;
	FVector TopLeft;
	FVector BottomRight;
	float Padding;
	AActor* SpawnItem(UClass* ItemToSpawn);
	AActor* Food;
	AActor* Debuff1;
	AActor* Debuff2;
};
